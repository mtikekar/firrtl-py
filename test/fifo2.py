import functools
from firrtl_py import *
import sys


@functools.cache
def FIFO2(width: int):
    T, Bool = UInt[width], UInt[1]
    m = Module(f"FIFO2_width{width}")

    clk = m.clk = In(Clock)
    rst = m.rst = In(Bool)

    m.wdata = In(T)
    m.wrdy = Out(Bool)
    m.wen = In(Bool)
    m.overflow = Out(Bool)

    m.rdata = Out(T)
    m.rrdy = Out(Bool)
    m.ren = In(Bool)
    m.underflow = Out(Bool)

    d0 = m.Reg(T, clk)
    d1 = m.Reg(T, clk)
    dv = m.Reg(UInt[2], clk, rst, 0)  # data valids

    ovf = m.Reg(Bool, clk, rst, 0)
    udf = m.Reg(Bool, clk, rst, 0)
    m.overflow @= ovf
    m.underflow @= udf

    ovf @= ovf | (m.wen & ~m.wrdy)
    udf @= udf | (m.ren & ~m.rrdy)

    # Possible states for dv:
    # 0 elements - (0, 0)
    # 1 element  - (1, 0)
    # 2 elements - (1, 1)
    # use a shift-register inside to get zero combinational delay on outputs
    m.rdata @= d0
    m.rrdy @= dv[0]
    m.wrdy @= ~dv[1]

    # TODO: do this with just ifs as conditions are mutually exclusive
    with m.If(m.wen & (dv == 0)):
        d0 @= m.wdata
        dv @= 1

    with m.Elif(dv == 1):
        with m.If(m.wen & ~m.ren):
            d1 @= m.wdata
            dv @= 3
        with m.Elif(m.wen & m.ren):
            d0 @= m.wdata
        with m.Elif(m.ren & ~m.wen):
            dv @= 3

    with m.Elif(m.ren & (dv == 3)):
        dv @= 1
        d0 @= d1

    return m


if __name__ == "__main__":
    m = FIFO2(5)
    sys.stdout.buffer.write(m._to_firrtl_proto().SerializeToString())