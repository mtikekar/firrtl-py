from firrtl_py import *

class Foo(Bundle):
    x: UInt[4]
    y: UInt[3] = 'flip'


class Bar(Bundle):
    y: UInt[5]
    z: Foo = 'flip'

class Baz(Foo):
    a: UInt[33]



class Module2(Module):
    def __init__(m, width):
        super().__init__(name=f"module2_width{width}")
        m.clk = In(Clock)
        m.x1 = In(UInt[width])
        m.x2 = Out(UInt[width])

        r1 = m.Reg(UInt[4], m.clk)
        w1 = m.Wire(UInt[2])

        r1 @= m.x1[3]
        w1 @= m.x2 + m.x1

        m.x2 @= r1


module2_p1 = Module2(5)
module2_p2 = Module2(8)


class Module3(Module):
    def __init__(m, width):
        super().__init__(name=f"module3_width{width}")
        m.clk = In(Clock)
        m.x1 = In(Foo)
        m.x2 = Out(Int[8])
        m.x3 = In(Bar)
        m.x3.z @= m.x1

        u1 = m.Inst(module2_p1, clk=m.clk)
        u2 = m.Inst(module2_p2, clk=m.clk)
        u3 = m.Inst(Module2(width), clk=m.clk, x1=u2.x2, x2=u2.x1)

        u1.x1 @= UInt[None](4)
        m.x2 @= 0

module3_p1 = Module3(5)

if __name__ == '__main__':
    import sys
    m = module3_p1
    #print(str(m._to_firrtl_proto()))
    sys.stdout.buffer.write(m._to_firrtl_proto().SerializeToString())
    sys.exit()

    def print_module(mod):
        print(mod._ports)
        print(mod._statements)

    print(module3_p1.u2.x2._to_proto())
    print(module3_p1.u2._to_proto())
    print(module3_p1.x1._to_proto())
    print(module3_p1.clk._to_proto())

    print(Clock._to_type_proto())
    print(UInt[3]._to_type_proto())
    print(UInt[3](4)._to_type_proto())

    print(Foo._to_type_proto())
    print(Bar._to_type_proto())
    print(Baz._to_type_proto())

    # print(module3_p1._to_firrtl_proto())


