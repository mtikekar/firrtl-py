# Code structure choices

Should the python repr. of the circuit use protobuf as object model?
 - Yes: Simple serialization: as you elaborate the module the protobuf gets created.
   Object model is cleanly defined in a schema and serialization is "automatic".
 - No: We might want flexibility to describe modules with unevaluated parameter types,
   then evaluate the parameters and then generate the protobuf.

Decision: No.

How should references be tracked for the purpose of serialization?
 - Python objects don't know the name with which they are instantiated in their parent.
   We need their full hierarchical name during serialization.
 - Option 1: Give names when they are instantiated in the parent. Not necessarily nice
   for programmatic use. e.g. if a component ends up with multiple references (will this
   happen in real life?). This can be done with explicit name, stack tracing like in
   amaranth, or with m.x = Reg(..) or with x = m.Reg()
 - Option 2: Get name by overriding getattr so that hierarchical name becomes available
   at read time.
 - Option 3: During or before serialization, traverse the object tree to give names.

   Ideally want to do x = Reg() within a namespace (e.g. like dataclass syntax), but
   also keep programmatic API simple.

Decision: Option 3 for now. May revisit later.

Should module objects be elaborated or can elaboration be deferred until after object
creation. Basically, should generators be module classes or functions that return module
classes?

Output parameters?


# Next steps

- Select initial subset of the syntax to support and add tests as features are added
- Mock up the serialization step with multiple modules.
- Think clearly about when params are evaluated and when the modules are built up. The
  params don't need to be evaluated every time a module is instantiated.
- Think about how to go back from python objects to their references. We don't need them
  for all objects - the only accessible id's are objects in current module and ports of
  sub-modules. Should modules only return their interface? Yes.
- 
