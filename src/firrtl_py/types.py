"""
Type syntax

  UInt[W] where W = None, 1, 2, 3, 4, ... (0?)
  Int[W]
  Fixed[W, F]
  Clock
  Vector[Type, N] - TODO

  class MyBundle(Bundle):
     x: Type
     y: Type = 'flip'

Create constants using UInt[4](14), etc. Create variables using
module.Reg, module.Wire, module.In, module.Out.

HW types are Python classes, HW constants and variables are instances
of those Python classes. UInt[1] and UInt[2] are different classes.
Two UInt[1]'s are the same class (at least in the same Python process).
"""

import inspect
from typing import ClassVar, Optional, NamedTuple

from . import expr, module, firrtl_pb2

Firrtl = firrtl_pb2.Firrtl
Expr = Firrtl.Expression

# TODO: document how these mixins work and clean up how the __init__ and
# as_ref functions work together
class RefMixin:
    @classmethod
    def _as_ref(cls, parent=None, name=None, sink=False):
        # parent and name may remain unintialized for now
        self = super().__new__(cls)
        self._value = None
        self._parent = parent
        self._name = name
        self._sink = sink
        return self

    @classmethod
    def _as_expr(cls, value):
        return value

    def __repr__(self):
        cls = type(self).__name__
        return f"{self._name}: {cls}"

    # TODO: this can be cached for speed
    def _get_module_ancestor(self):
        p = self._parent
        while not isinstance(p, module.Module):
            p = p._parent
        return p

    def __imatmul__(self, other):
        if other is None:
            stmt = {"is_invalid": {"expression": self}}
        else:
            stmt = {"connect": {"location": self, "expression": self._as_expr(other)}}
        self._get_module_ancestor()._add_statement(**stmt)
        return self
        # TODO: check sink status
        # TODO: use different syntax for wire

    def _to_proto(self) -> Expr:
        if isinstance(self._parent, module.Module):
            return Expr(reference=Expr.Reference(id=self._name))
        if isinstance(self._name, str):
            return Expr(sub_field=Expr.SubField(
                expression=self._parent._to_proto(),
                field=self._name))
        if isinstance(self._name, int):
            return Expr(sub_index=Expr.SubIndex(
                expression=self._parent._to_proto(),
                index=integer_literal(self._name)))
        return super()._to_proto()


class _UInt(expr.ExprMixin, RefMixin):
    _width: ClassVar[Optional[int]]

    def __init__(self, value: int):
        w = self._width
        if w is None or 0 <= value < (1 << w):
            self._value = value
        else:
            raise ValueError(f"Invalid literal {value} for UInt[{w}]")

    @classmethod
    def _to_type_proto(cls) -> Firrtl.Type:
        return Firrtl.Type(uint_type={"width": expr.width(cls._width)})

    def _to_proto(self) -> Expr:
        if isinstance(self._value, int):
            return Expr(uint_literal={
                "value": expr.integer_literal(self._value),
                "width": expr.width(self._width)})
        return super()._to_proto()


class _Int(expr.ExprMixin, RefMixin):
    _width: ClassVar[Optional[int]]

    def __init__(self, value: int):
        w = self._width
        if w is None or -(1 << w-1) <= value < (1 << w-1):
            self._value = value
        else:
            raise ValueError(f"Invalid literal {value} for Int[{w}]")

    @classmethod
    def _to_type_proto(cls) -> Firrtl.Type:
        return Firrtl.Type(sint_type={"width": expr.width(cls._width)})

    def _to_proto(self) -> Expr:
        if isinstance(self._value, int):
           return Expr(sint_literal={
               "value": expr.integer_literal(self._value),
               "width": expr.width(self._width)})
        return super()._to_proto()


class _FixedPt(expr.ExprMixin, RefMixin):
    _width: ClassVar[Optional[int]]
    _frac: ClassVar[Optional[int]]

    def __init__(self, value: int):
        w = self._width
        if w is None or -(1 << w-1) <= value < (1 << w-1):
            self._value = value
        else:
            raise ValueError(f"Invalid literal {value} for FixedPt[{w}, {self._frac}]")


    @classmethod
    def from_float(cls, x:float, approx=False):
        y = x * (2 ** cls._frac)
        yi = round(y)
        if approx is False and y != yi:
            raise ValueError(f"Rounding error for {y}")
        return cls(yi)

    def to_float(self):
        return self._value / (2 ** self._frac)

    @classmethod
    def _to_type_proto(cls) -> Firrtl.Type:
        return Firrtl.Type(fixed_type={"width": expr.width(cls._width),
                                       "point": expr.width(cls._frac)})

    def _to_proto(self) -> Expr:
        if isinstance(self._value, int):
            return Expr(fixed_literal={"value": expr.big_int(self._value),
                                       "width": expr.width(self._width),
                                       "point": expr.width(self._frac)})
        return super()._to_proto()


class Clock(RefMixin):
    # TODO: check that only name and not value is provided
    @classmethod
    def _to_type_proto(cls) -> Firrtl.Type:
        return Firrtl.Type(clock_type={})


class _BundleField(NamedTuple):
    type_: RefMixin
    is_flipped: bool


# TODO: make a separate type for types that have literals and perhaps also for
# types that are always "passive" (no combination of flipped and non-flipped fields)
# TODO: Better programmatic bundle creation and check how IDEs handle dynamic classes.
class Bundle(RefMixin):
    @classmethod
    def _as_ref(cls, parent=None, name=None, sink=False):
        self = super()._as_ref(parent, name, sink)
        for name, field in cls._get_all_bundle_fields().items():
            _sink = sink ^ field.is_flipped
            subfield = field.type_._as_ref(self, name, _sink)
            setattr(self, name, subfield)
        return self

    # TODO: this should probably be computed once and cached
    @classmethod
    def _get_bundle_fields(cls) -> dict[str, _BundleField]:
        fields = {}
        for name, type_ in inspect.get_annotations(cls).items():
            flipped = False
            if hasattr(cls, name):
                attrval = getattr(cls, name)
                if attrval != 'flip':
                    raise ValueError(f"Unknown value {attrval} for field {name}. Expected 'flip'.")
                flipped = True
            fields[name] = _BundleField(type_=type_, is_flipped=flipped)
        return fields

    # TODO: this should probably be computed once and cached
    @classmethod
    def _get_all_bundle_fields(cls) -> dict[str, _BundleField]:
        fields = {}
        # bundles can inherit from other bundles
        for base in cls.__mro__[::-1]:
            if issubclass(base, Bundle):
                fields.update(base._get_bundle_fields())
        return fields

    @classmethod
    def _to_type_proto(cls) -> Firrtl.Type:
        msg = Firrtl.Type(bundle_type={})
        for name, field in cls._get_bundle_fields().items():
            msg.bundle_type.field.add(
                    is_flipped=field.is_flipped,
                    id=name,
                    type=field.type_._to_type_proto())
        return msg

    @classmethod
    def _to_ports_proto(cls) -> list[Firrtl.Port]:
        l = []
        for name, field in cls._get_bundle_fields().items():
            l.append(Firrtl.Port(
                direction='PORT_DIRECTION_' + ('IN' if field.is_flipped else 'OUT'),
                id = name,
                type = field.type_._to_type_proto()))
        return l


# Unique type generator
def _as_tpl(x):
    if isinstance(x, (list, tuple)):
        return tuple(x)
    return (x,)

class _TypeCache:
    def __init__(self, base_type, type_var_names, type_name_fmt):
        self.base_type = _as_tpl(base_type)
        self.type_var_names = _as_tpl(type_var_names)
        self.type_name_fmt = type_name_fmt
        self.types = {}

    def __getitem__(self, key):
        if key in self.types:
            return self.types[key]
        type_dict = dict(zip(self.type_var_names, _as_tpl(key)))
        type_name = self.type_name_fmt.format(**type_dict)
        self.types[key] = new_type = type(type_name, self.base_type, type_dict)
        return new_type


UInt = _TypeCache(_UInt, "_width", "UInt[{_width}]")
Int = _TypeCache(_Int, "_width", "Int[{_width}]")
FixedPt = _TypeCache(_FixedPt, ("_width", "_frac"), "FixedPt[{_width}, {_frac}]")